import React from 'react'
import './header.css'

export default function Header() {
  return (
    <div className="header">
      <div className="logo">
        <h1>Logo</h1>
      </div>
      <div className="user">
        <h2>Hi, <span style={{color:"blue"}}>User</span></h2>
      </div>
    </div>
  )
}
