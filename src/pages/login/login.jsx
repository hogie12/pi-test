import axios from "axios";
import React, { useState } from "react";
import { Navigate } from "react-router";
import "./login.css";

export default function LoginPage() {
  const [isLogin, setIsLogin] = useState(null);
  const [isError, setIsError] = useState(null);
  const [message, setMessage] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const login = () => {
    const data = { username, password };
    axios
      .post("https://tasklogin.herokuapp.com/api/login", data)
      .then((response) => {
        setIsLogin(true);
        setMessage("");
      })
      .catch((error) => {
        setIsError(true);
        setMessage(error.response.data.message);
      });
  };
  const handleLogin = (e) => {
    e.preventDefault();
    login();
  };

  if (isLogin) {
    return <Navigate to="/homepage" />;
  }

  return (
    <div className="login-page">
      <div className="form">
        <h1>Login</h1>
        <form className="login-form" onSubmit={handleLogin}>
          <input
            type="text"
            placeholder="username"
            value={username}
            onChange={(e) => {
              setUsername(e.target.value);
            }}
          />
          <input
            type="password"
            placeholder="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          {isError ? <p className="message">* {message}</p> : null}
          <button>login</button>
        </form>
      </div>
    </div>
  );
}
