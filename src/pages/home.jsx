import React from "react";
import Header from "../component/header/header";

export default function HomePage() {
  return (
    <div>
      <Header />
      <div style={{ textAlign: "center" }}>
        <h1>Home</h1>
      </div>
    </div>
  );
}
